# Getting Started

### In root directory :
```dockerfile
docker-comopose -f docker-compose.yml up -d
```
### Run application
```bash
 ./gradlew run
```
### Go to:
```
localhost:8081/hello
```
### Use this credentials:
```json
login: admin
password: admin
```